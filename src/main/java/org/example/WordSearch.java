package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class WordSearch {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Senay\\Desktop\\AlphabetSoup\\src\\main\\resources\\input.txt"))) {
            // Read the grid dimensions from the first line
            String dimensionsLine = reader.readLine();
            String[] dimensions = dimensionsLine.split("x");
            int numRows = Integer.parseInt(dimensions[0]);
            int numCols = Integer.parseInt(dimensions[1]);

            // Read the grid of characters
            char[][] grid = new char[numRows][numCols];
            for (int i = 0; i < numRows; i++) {
                String rowLine = reader.readLine();
                String[] characters = rowLine.split(" ");
                for (int j = 0; j < numCols; j++) {
                    grid[i][j] = characters[j].charAt(0);
                }
            }

            // Read the list of words to find
            String line;
            while ((line = reader.readLine()) != null) {
                String[] wordsToFind = line.split(" ");
                for (String word : wordsToFind) {
                    searchAndPrintWord(grid, word);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Searches for a word in all directions within the grid and prints its location.
     *
     * @param grid The grid of characters to search in.
     * @param word The word to find.
     */
    private static void searchAndPrintWord(char[][] grid, String word) {
        int numRows = grid.length;
        int numCols = grid[0].length;

        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                // Check horizontal right
                if (col + word.length() <= numCols) {
                    boolean found = true;
                    for (int i = 0; i < word.length(); i++) {
                        if (grid[row][col + i] != word.charAt(i)) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        System.out.println(word + " " + row + ":" + col + " " + row + ":" + (col + word.length() - 1));
                        return;
                    }
                }

                // Check horizontal left
                if (col - word.length() + 1 >= 0) {
                    boolean found = true;
                    for (int i = 0; i < word.length(); i++) {
                        if (grid[row][col - i] != word.charAt(i)) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        System.out.println(word + " " + row + ":" + col + " " + row + ":" + (col - word.length() + 1));
                        return;
                    }
                }

                // Check vertical down
                if (row + word.length() <= numRows) {
                    boolean found = true;
                    for (int i = 0; i < word.length(); i++) {
                        if (grid[row + i][col] != word.charAt(i)) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        System.out.println(word + " " + row + ":" + col + " " + (row + word.length() - 1) + ":" + col);
                        return;
                    }
                }

                // Check vertical up
                if (row - word.length() + 1 >= 0) {
                    boolean found = true;
                    for (int i = 0; i < word.length(); i++) {
                        if (grid[row - i][col] != word.charAt(i)) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        System.out.println(word + " " + row + ":" + col + " " + (row - word.length() + 1) + ":" + col);
                        return;
                    }
                }

                // Check diagonal directions (down-right, down-left, up-right, up-left)
                if (col + word.length() <= numCols && row + word.length() <= numRows) {
                    boolean found = true;
                    for (int i = 0; i < word.length(); i++) {
                        if (grid[row + i][col + i] != word.charAt(i)) {
                            found = false;
                            break;
                        }
                    }
                    if (found) {
                        System.out.println(word + " " + row + ":" + col + " " + (row + word.length() - 1) + ":" + (col + word.length() - 1));
                        return;
                    }
                }
            }
        }
    }
}
